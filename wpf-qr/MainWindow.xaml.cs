﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf_qr;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window, INotifyPropertyChanged
{
    QrService m_qrService;

    string m_dataStr;
    Bitmap? m_qrBitmap;
    BitmapSource? m_qrBitmapSource;

    public event PropertyChangedEventHandler? PropertyChanged;

    public string DataStr
    {
        get => m_dataStr;
        set
        {
            m_dataStr = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(DataStr)));
        }
    }

    public BitmapSource? QrBitmapSource
    {
        get => m_qrBitmapSource;
        set
        {
            m_qrBitmapSource = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(QrBitmapSource)));
        }
    }

    public ICommand GenerateQrCommand
    {
        get => new RelayCommand(GenerateQr);
    }

    public ICommand SaveQrCommand
    {
        get => new RelayCommand(SaveQr);
    }

    public MainWindow()
    {
        InitializeComponent();
        DataContext = this;

        m_qrService = new QrService();
        m_dataStr = "";

        Unloaded += HandleUnload;
    }

    void HandleUnload(object sender, RoutedEventArgs e)
    {
        m_qrBitmap?.Dispose();
        m_qrBitmap = null;
    }

    void GenerateQr()
    {
        if (string.IsNullOrEmpty(DataStr))
        {
            MessageBox.Show(
                "No data to be encoded",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        m_qrBitmap?.Dispose();
        m_qrBitmap = m_qrService.GenerateQr(
            DataStr,
            System.Drawing.Color.Black,
            System.Drawing.Color.Transparent);
        
        QrBitmapSource = Imaging.CreateBitmapSourceFromHBitmap(
            m_qrBitmap.GetHbitmap(),
            IntPtr.Zero,
            Int32Rect.Empty,
            BitmapSizeOptions.FromEmptyOptions());
    }

    void SaveQr()
    {
        if (m_qrBitmap == null)
        {
            MessageBox.Show(
                "Nothing is encoded yet",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        SaveFileDialog dlg = new SaveFileDialog();
        dlg.Filter = "Image (*.png)|*.png";
        dlg.DefaultExt = "png";
        bool? success = dlg.ShowDialog();

        if (success != true)
        {
            return;
        }

        m_qrBitmap.Save(
            dlg.FileName,
            ImageFormat.Png);
    }
}
